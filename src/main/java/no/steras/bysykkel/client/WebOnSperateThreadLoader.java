package no.steras.bysykkel.client;

import java.util.ArrayList;
import java.util.List;

import no.steras.bysykkel.client.data.Backend;
import no.steras.bysykkel.client.data.Station;
import no.steras.bysykkel.client.data.StationsOpenHelper;

import org.json.JSONException;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.AsyncTaskLoader;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Tracker;
import com.google.android.gms.maps.model.LatLng;

public class WebOnSperateThreadLoader extends AsyncTaskLoader<List<Station>> {

	private Backend backend;
	private final Timer stopWatch;
	private StationsOpenHelper stationsHelper;
	private Tracker GATracker;

	public WebOnSperateThreadLoader(Context context) {
		super(context);
		backend = new Backend();
		stopWatch = new Timer();
		stationsHelper = new StationsOpenHelper(context);
		GATracker = EasyTracker.getTracker();
	}

	@Override
	public List<Station> loadInBackground() {
		try {
			return loadStations();
		} catch (JSONException e) {
			throw new RuntimeException(e);
		}
	}

	private List<Station> loadStations() throws JSONException {
		stopWatch.start();
		Map<Integer, Station> stations = new HashMap<Integer, Station>();

		backend.loadData();

		Cursor c = stationsHelper.getStations();
		c.moveToFirst();
		while (c.isAfterLast() == false) {
			Station station = createStationFromDBData(c);

			backend.populateWithBackEndData(station);

			stations.put(stations.getId(), station);
			c.moveToNext();
		}
		c.close();

		collapseStations(stations);
		GATracker.sendTiming("Initial loading time", stopWatch.getElapsedTime(), "loadStations", null);
		return new ArrayList(stations.values);

	}
	
	private void collapseStations(Map<Integer, Station> stationsMap) {
        for (List<Integer> collapse : configuration.getCollapseConfiguration()) {
            Station collapsedStation = new Station();
            Station firstStation = stationsMap.get(collapse.get(0));
            Station secondStation = stationsMap.get(collapse.get(1));

            if (firstStation != null && secondStation != null) {

                log.info("Collapsing stations " + firstStation.getId() + " and " + secondStation.getId());
                collapsedStation.setId(Integer.parseInt("100" + firstStation.getId() + secondStation.getId()));
                collapsedStation.setBikesReady(firstStation.getBikesReady() + secondStation.getBikesReady());
                collapsedStation.setEmptyLocks(firstStation.getEmptyLocks() + secondStation.getEmptyLocks());
                collapsedStation.setOpen(firstStation.getOpen() | secondStation.getOpen());
                collapsedStation.setOnline(firstStation.getOnline() | secondStation.getOnline());
                collapsedStation.setLongitude(firstStation.getLongitude());
                collapsedStation.setLatitude(firstStation.getLatitude());
                collapsedStation.setDescription(firstStation.getDescription());

                stationsMap.put(collapsedStation.getId(), collapsedStation);

                if (configuration.getRemoveCollapsedStation()) {
                    stationsMap.remove(firstStation.getId());
                    stationsMap.remove(secondStation.getId());

                }
            }

        }
    }

	private List<List<Integer>> getCollapseConfiguration() {			
			List<List<Integer>> collapseConfiguration = new ArrayList<List<Integer>>();
			List<Integer> collapse1 = new ArrayList<Integer>();
			collapse1.add(7);
			collapse1.add(8);
			collapseConfiguration.add(collapse1);
			List<Integer> collapse2 = new ArrayList<Integer>();
			collapse2.add(27);
			collapse2.add(99);
			collapseConfiguration.add(collapse2);
			List<Integer> collapse4 = new ArrayList<Integer>();
			collapse4.add(9);
			collapse4.add(10);
			collapseConfiguration.add(collapse4);
			List<Integer> collapse5 = new ArrayList<Integer>();
			collapse5.add(50);
			collapse5.add(51);
			collapseConfiguration.add(collapse5);
			List<Integer> collapse6 = new ArrayList<Integer>();
			return collapseConfiguration;
	}
	
	private Station createStationFromDBData(Cursor c) {
		int latitudeColumnIndex = c.getColumnIndex("latitude");
		int longitudeColumnIndex = c.getColumnIndex("longitude");
		int descriptionColumnIndex = c.getColumnIndex("description");
		int idColumnIndex = c.getColumnIndex("_id");

		Station station = new Station();
		station.setLocation(new LatLng(c.getDouble(latitudeColumnIndex), c.getDouble(longitudeColumnIndex)));

		station.setDescription(c.getString(descriptionColumnIndex));
		station.setId(c.getInt(idColumnIndex));

		return station;
	}

}
